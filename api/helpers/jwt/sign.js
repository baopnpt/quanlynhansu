const jwt = require('jsonwebtoken'), moment = require('moment');

module.exports = {


  friendlyName: 'Sign',


  description: 'Sign jwt.',


  inputs: {
    member: { type: 'ref', description: 'Thông tin người dùng', required: true },
    time: { type: 'string', description: 'Thời gian hết hạn. Đơn vị giừ', defaultsTo: '60 days' }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },

  sync: true,
  fn: function (inputs, exits) {
    let { member, time } = inputs;
    let token = jwt.sign(member, "secret", { expiresIn: time });
    return exits.success(token);
  }
};

