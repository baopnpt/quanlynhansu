const moment = require('moment');

module.exports = {

  friendlyName: 'Login',


  description: 'Login member.',


  inputs: {
    username: {type: 'string'},
    password: {type: 'string'}
  },


  exits: {
    success: {
      statusCode: 200,
    },
    fail: {
      statusCode: 400
    }
  },


  fn: async function (inputs, exits) {

    let { username, password } = inputs;
    if(!username || !password){
      return exits.fail({
        success: 0,
        msg: 'Không đủ thông tin đăng nhập'
      })
    }
    let memberInfo = await Membership.findOne({username});
    if(!memberInfo) {
      return exits.fail({
        success: 0,
        msg: 'Thành viên không tồn tại'
      })
    }
    // if(!sails.helpers.common.checkHash.with({
    //   text: password,
    //   hash: memberInfo.password
    // })){
    //   return exits.fail({
    //     success: 0,
    //     msg: 'Mật khẩu không đúng'
    //   })
    // }
    if(password !== memberInfo.password){
      return exits.fail({
        success : 0,
        msg : 'Mật khẩu không đúng'
      })
    }

    delete memberInfo.password; 

    let token = sails.helpers.jwt.sign.with({
      member: {
        id: memberInfo.id,
        //options: memberInfo.options
      },
      time: sails.config.TOKEN_TIME
    })

    return exits.success({
      success: 1,
      msg: 'Thành công',
      data: {
        memberInfo,
        token,
        expiredAt: moment().add(sails.config.TOKEN_TIME, 'hours').valueOf()
      }
    })
  }
};
