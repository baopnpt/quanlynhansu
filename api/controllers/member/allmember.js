module.exports = {


  friendlyName: 'Allmember',


  description: 'Allmember member.',


  inputs: {
    skip : {type : 'number'},
    limit : {type : 'number'},
    search :{type:'string'}
  },


  exits: {
    success : {},
    fail : {}
  },


  fn: async function (inputs,exits) {

    try {
      let {skip , limit, search} = inputs;
      let sex = search;
      if(search === 'nam' || search === 'Nam' || search === 'Na' || search === 'na') sex = 1;
      if(search === 'nu' || search === 'Nữ' || search === 'nữ') sex = 0
      let templimit = limit ? `limit ${limit}` : '';
      let tempskip = skip ? `offset ${skip}` : '';
      let tempsearch = search ? `WHERE
      membership.fullname LIKE '%${search}%' OR
      membership.phone LIKE '%${search}%' OR
      membership.birthday LIKE '%${search}%' OR
      membership.sex LIKE '%${sex}%' OR
      membership.address LIKE '%${search}%' OR
      position.namePosition Like '%${search}%' OR
      literacy.nameLiteracy Like '%${search}%' OR
      salary.levelSalary Like '%${search}%' OR
      department.nameDpm Like '%${search}%' ` : '';
      let sql =  `SELECT
      membership.id,
      membership.fullname,
      membership.sex,
      membership.phone,
      membership.birthday,
      membership.address,
      position.namePosition,
      literacy.nameLiteracy,
      salary.levelSalary,
      department.nameDpm 
    FROM
      membership
      INNER JOIN position on membership.positionId = position.id
      INNER JOIN literacy on membership.literacyId = literacy.id
      INNER JOIN salary on membership.salaryId = salary.id
      INNER JOIN department ON membership.departmentId = department.id
      ${tempsearch}
      ${templimit} ${tempskip}`;
      let count = `SELECT
      Count(*) as tong
      FROM
      membership
      INNER JOIN position on membership.positionId = position.id
      INNER JOIN literacy on membership.literacyId = literacy.id
      INNER JOIN salary on membership.salaryId = salary.id
      INNER JOIN department ON membership.departmentId = department.id
      ${tempsearch}`;
      let raw = await Promise.all([
        sails.sendNativeQuery(sql),
        sails.sendNativeQuery(count)
      ]);
      
      return exits.success({
        code : 0,
        data : raw[0].rows,
        total : raw[1].rows[0].tong
      })
    } catch (error) {
      console.log(error);
      
      return exits.fail({
        code : 1,
        data : []
      })
    }
  }
};
