module.exports = {
  friendlyName: "Findmember",

  description: "Findmember member.",

  inputs: {
    id: { type: "string", require: true }
  },

  exits: {
    success: {},
    fail: {}
  },

  fn: async function(inputs, exits) {
    try {
      let {id} = inputs;
      let rs = await Membership.find({id});
      exits.success({
        code : 0,
        data : rs
      })
    } catch (error) {
      exits.fail({
        code : 1,
        msg : "Không tìm thấy nhân viên"
      })
    }
  }
};
