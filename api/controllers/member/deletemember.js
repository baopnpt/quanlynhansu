module.exports = {


  friendlyName: 'Deletemember',


  description: 'Deletemember member.',


  inputs: {
    id : {type : 'number'}
  },


  exits: {
    success : {},
    fail : {}
  },


  fn: async function (inputs,exits) {

    try {
      const {id} = inputs;
      await Membership.destroyOne({id});
      return exits.success({
        code : 0,
        msg: 'Thành công'
      })
    } catch (error) {
      return exits.fail({
        code : 1,
        msg: 'Thất bại'
      })
    }
  }


};
