
module.exports = {

  attributes: {
    levelSalary : {type : 'string'}, // bậc lương
    basicSalary : {type : 'number'}, //lương cơ bản
    coefficientsSalary : {type : 'number'}, //hệ số lương
    allowanceCoefficient : {type : 'number'}, //hệ số phụ cấp

    // memberships : {
    //   collection : 'membership',
    //   via : 'salary'
    // }
  },

};

