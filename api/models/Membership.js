/**
 * Membership.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    username: { type: "string" },
    password: { type: "string" },
    fullname : {type : "string"},
    sex : {type:"boolean"},
    birthday : {type:"number"},
    phone : {type : "string"},
    address : {type:"string"},
    positionId : {type : "string"},
    departmentId : {type : "string"},
    salaryId : {type : "string"},
    literacyId : {type : "string"}
    // department : {
    //   model : 'department'
    // },
    // salary : {
    //   model : 'salary'
    // },
    // position : {
    //   model : 'position'
    // },
    // literacys : {
    //   model: 'literacy'
    // }
  }
};
